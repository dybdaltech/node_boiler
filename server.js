//Package dependencies
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const colors = require('colors');
const bodyParser = require('body-parser');
//File dependencies
const config = require('./config/config');


//Middleware
let app = express();
app.use(cors());
app.use(bodyParser());

//Database connections:
mongoose.connect(config.mongoURL);
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Database connection error'));

db.on('open', (callback) => {
    console.log(colors.green('Database connection succesfull'));
});

//Routers
let route_main = require('./routes/main');


app.use('/', route_main);

app.listen(config.PORT, () => console.log('API server running on port: ' + colors.red(config.PORT)));